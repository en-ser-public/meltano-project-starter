# Meltano Starter Project

This project is merely a starter for using Meltano, Apache Superset via Juju.  You can clone it and customize your own..
but it's likely best you go directly to the latest meltano.com for the latest

# How-To

The best place to start using this starter is the Juju Charm bundle located at

https://gitlab.com/jrgemcp-public/charms/meltano-superset-postgres-bundle-charm